import './App.css';
import api from './api/axiosConfig';
import {useState, useEffect} from 'react';
import Layout from './components/Layout';
import {Routes, Route} from 'react-router-dom';


  function App(){

    const [bookings, setBookings] = useState();

    const getBookings = async () =>{
        try
        {
          const config = {withCredentials: false};
          const response = await api.get("/api/bookings", config);

          setBookings(response.data);

        }
        catch(err)
        {
          console.log(err);
        }
    }

    useEffect(() =>{
        getBookings();
    },[])

    return (
        <div className="App">

            <Routes></Routes>

        </div>
    );
  }
export default App;

