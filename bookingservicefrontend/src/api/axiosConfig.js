import axios from 'axios';

export default axios.create({
    baseURL:'https://1870-131-227-23-35.eu.ngrok.io',
    withCredentials: true
});