package com.parkingsolution.bookingservice.controller;

import com.parkingsolution.bookingservice.model.Booking;
import com.parkingsolution.bookingservice.model.Duration;
import com.parkingsolution.bookingservice.repository.BookingRepo;
import com.parkingsolution.bookingservice.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/bookings")
@CrossOrigin(origins = "http://localhost:3000")
public class BookingController {

    @Autowired
    private BookingService bookingservice;

    @GetMapping
    public ResponseEntity<List<Booking>> getBooking(){
        List<Booking> bookings = bookingservice.findAllBookings();
        HttpHeaders headers = new HttpHeaders();
        headers.add("All-Bookings", "header value");
        return new ResponseEntity<List<Booking>>(bookings, headers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Booking>> getBookingById(@PathVariable String refNo){
        Optional<Booking> booking = bookingservice.singleBooking(refNo);
        HttpHeaders headers = new HttpHeaders();
        headers.add("SingleBooking", "value");
        return new ResponseEntity<>(booking, headers, HttpStatus.OK);
    }

//    @GetMapping("/{locationId}")
//    public ResponseEntity<List<Booking>> getBookingByLocation(@PathVariable String locationId){
//        return new ResponseEntity<List<Booking>>(bookingservice.allBookingsInLocation(locationId), HttpStatus.OK);
//    }

    @PostMapping
    public ResponseEntity<Booking> createBooking(@RequestBody Booking booking){
        Booking newBooking = bookingservice.createBooking(booking);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Create-Booking", "value");

        return ResponseEntity.ok()
                .headers(headers)
                .body(newBooking);
    }

    @GetMapping("/duration")
    public Duration getDuration(@RequestParam Duration duration) {
        return duration;
    }


    @PutMapping("/{refNo}")
    public ResponseEntity<?> updateBooking(@PathVariable String refNo,
                                           @RequestBody Booking updatedBooking,
                                           @RequestHeader HttpHeaders headers) {
        Optional<Booking> optionalBooking = bookingservice.singleBooking(refNo);

        if (optionalBooking.isPresent()) {
            Booking booking = optionalBooking.get();

            // Update end time and duration
            booking.setDuration(updatedBooking.getDuration());
            booking.setEndTime(booking.getStartTime().plusMinutes(updatedBooking.getDuration().getMin()));
            // Save updated booking to database
            bookingservice.saveBooking(booking);

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Extend-Booking", "Value");
            return ResponseEntity.ok()
            .headers(responseHeaders)
                    .body(booking);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete/{refNo}")
    public ResponseEntity<Void> deleteBooking(@PathVariable String refNo, @RequestHeader("Authentication") String token){
        bookingservice.deleteBookingById(refNo);
        return ResponseEntity.noContent().build();
    }

}

// added getBookingByLocation
// changed updateBooking endpoint to make it simpler
// fixed bug when updating duration endpoint would return null
// changed controller to allow HttpHeaders
