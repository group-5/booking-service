package com.parkingsolution.bookingservice.repository;

import com.parkingsolution.bookingservice.model.Booking;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepo extends MongoRepository<Booking, String> {
//    List<Booking> findAll(String locationId);
}
