package com.parkingsolution.bookingservice.repository;

import com.parkingsolution.bookingservice.model.Booking;
import com.parkingsolution.bookingservice.model.Slots;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SlotsRepo extends MongoRepository<Slots, String> {
    Optional<Slots> findByBooking(Booking booking);
//    int countByLocationId(String locationId);
}

// Added countByLocationId which counts all slots associated with that location
