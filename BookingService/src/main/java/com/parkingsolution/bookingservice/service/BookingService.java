package com.parkingsolution.bookingservice.service;


import com.parkingsolution.bookingservice.model.Booking;
import com.parkingsolution.bookingservice.model.Duration;
//import com.parkingsolution.bookingservice.model.Location;
import com.parkingsolution.bookingservice.model.LocationResponse;
import com.parkingsolution.bookingservice.model.Slots;
import com.parkingsolution.bookingservice.repository.BookingRepo;
import com.parkingsolution.bookingservice.repository.SlotsRepo;
//import com.parkingsolution.bookingservice.repository.LocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.xml.stream.Location;
import java.awt.print.Book;
import java.time.LocalTime;
import java.util.*;

@Service
public class BookingService {
    @Autowired
    private BookingRepo bookingRepo;

    @Autowired
    private SlotsRepo slotsRepo;

    private final RestTemplate restTemplate;

    public BookingService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Booking> findAllBookings() {
        return bookingRepo.findAll();
    }

    public Optional<Booking> singleBooking(String refNo) {
        return bookingRepo.findById(refNo);
    }

//    public List<Booking> allBookingsInLocation(String locationId) {
//        return bookingRepo.findAll(locationId);
//    }


    public Booking createBooking(Booking booking) {
        String locationEndpoint = "LOCATION ENDPOINT HERE" + booking.getLocationId();
        String durationEndpoint = "https://96e5-131-227-23-35.eu.ngrok.io/api/bookings/duration?duration=" + booking.getDuration().name();

        booking.setRefNo(UUID.randomUUID().toString());
        booking.setStartTime(LocalTime.now());
        Duration duration = restTemplate.getForObject(durationEndpoint, Duration.class);
        booking.setDuration(duration);
        booking.setEndTime(LocalTime.now().plusMinutes(booking.getDuration().getMin()));

        LocationResponse locationResponse = restTemplate.getForObject(locationEndpoint, LocationResponse.class);
        String locationTitle = locationResponse.getTitle();
        booking.setLocationId(locationTitle);

        booking = bookingRepo.save(booking);

        Slots slots = new Slots();
        slots.setSlotId(booking.getRefNo());
        slots.setBooking(booking);
        slotsRepo.save(slots);

        sendBookingInfo(booking);

        return booking;
    }

    public void sendBookingInfo(Booking booking){
        String notifEndpoint = "NOTIFICATION ENDPOINT HERE";
        String locationEndpoint = "LOCATION ENDPOINT HERE" + booking.getLocationId();

        ResponseEntity<Location> locationResponse = restTemplate.getForEntity(locationEndpoint, Location.class);
        Location location = locationResponse.getBody();

        BookingNotif<Booking, Location> request = new BookingNotif<>(booking, location);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<BookingNotif<Booking, Location>> requestEntity = new HttpEntity<>(request, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(notifEndpoint, requestEntity, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            System.out.println("Booking information sent successfully");
        } else {
            System.out.println("Failed to send booking information");
        }

    }
    public class BookingNotif<T, U> {
        private T booking;
        private U location;

        public BookingNotif() {
        }

        public BookingNotif(T booking, U location) {
            this.booking = booking;
            this.location = location;
        }

    }

//    public Location checkAvailability(Location location, Booking booking){
//        int count = slotsRepo.countByLocationLocationId(booking.getLocationId());
//        int maxCap = location.getMaxCapacity(); // taking max cap attribute from location class
//        // if count is greater or equal to mac cap in location class set availability in booking class to false and save to booking repo
//        if (count >= maxCap) {
//            booking.setAvailability(false);
//    }

    public void saveBooking(Booking booking) {
        bookingRepo.save(booking);
    }



    public void deleteBookingById(String refNo) {
        bookingRepo.deleteById(refNo);
    }


    @Scheduled(fixedRate = 3600000)
    public void deleteExpiredBookings() {
        List<Booking> bookings = bookingRepo.findAll();
        for (Booking booking : bookings) {
            if (booking.getEndTime().isBefore(LocalTime.now())) {
                Optional<Slots> slots = slotsRepo.findByBooking(booking);
                if (slots.isPresent()) {
                    slotsRepo.delete(slots.get());
                }
            }
        }
    }



// FIX FIX FIX FIX
//    public boolean checkAvailability(Location location, Booking booking) {
//        int count = slotsRepo.countByLocationId(location.getLocationId());
//        int maxCap = location.getMaxCapacity(); // taking max cap attribute from location class
//        // if count is greater or equal to mac cap in location class set availability in booking class to false and save to booking repo
//        if (count >= maxCap) {
//            return false;
//        }
//        return true;
//    }


}

// Set SlotId to retrieve the refNo as the slotId
// added getAllBookingsByLocation funcation that gets all bookings associated with that locationId
// added the sendBookingInfo method to interact with the Notification service by sending combined info or the booking and location
// added bookingNotif method so it takes in 2 parameters (booking and location)
// modified createBooking so now it uses REST API calls to locatoin service to retreive locatoin information and set locationId as Title
// REST API calls taken by POJO class LocationResponse
