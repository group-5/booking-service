package com.parkingsolution.bookingservice.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalTime;

@Data
@Builder
@Document(collection = "Booking")
@NoArgsConstructor
@AllArgsConstructor
/*Attributes for booking class**/
public class Booking {
    @Id
    private String refNo; //Booking ID and unique for each booking(PK).
    private Duration duration;
    private LocalTime startTime;
    private LocalTime endTime;
    private boolean availability;
    private String locationId;

//    @DBRef
//    private String userId; // Will be added later when user service is combined

}

// deleted dateOfBooking
