package com.parkingsolution.bookingservice.model;

public enum Duration {
    thirty_min(30),
    one_hour(60),
    ninety_min(90),
    two_hour(120);

    private final int min;

    Duration(int min) {
        this.min = min;
    }

    public int getMin() {
        return min;
    }
}
