package com.parkingsolution.bookingservice.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalTime;

@Data
@Builder
@Document(collection = "Slots")
@NoArgsConstructor
@AllArgsConstructor
public class Slots {
    @Id
    private String slotId;
    @DBRef
    private Booking booking;
}


