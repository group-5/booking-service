package com.parkingsolution.bookingservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.parsing.Location;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LocationResponse {
    private String title;
    private String description;
    private String city;
    private String street_address;
    private String postcode;
    private Location location;
    private int spaces_available;
    private int total_spaces;

}
